---
layout: handbook-page-toc
title: "Sales Quick Start (SQS) Workshop"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sales Quick Start Workshop
*  NOTE: The Sales Quick Start (SQS) Workshop is MANDATORY for all new Sales Team Members. If you must cancel for any reason, please be sure to obtain Regional Director approval in writing (Ryan O'Nell for Commercial).
*  After completing the virtual, self-paced Sales Quick Start learning path, new sales team members (along with new Solution Architects, Technical Account Managers, and Sales Development Rep) participate in an interactive in-person workshop where participants practice applying this new knowledge in fun and challenging ways including mock calls, role plays, and group activities
   - As a result, new sales team members exit Sales Quick Start more confident and capable in their ability to lead an effective discovery call with a customer or prospect
   - Participants also benefit by establishing a community of similarly-tenured peers and more experienced mentors and colleagues to support each others' growth and development at GitLab
*  **Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery)**
   - Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way
*  Future iteration of this process will define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## Sales Quick Start In-Person Workshop Agenda

This [SQS 2021 Agenda sheet](https://docs.google.com/spreadsheets/d/1f1O2VC_6Fjdhrpyi9vB81kvdJ4H-66F8ghv-h_-_bGw/edit?usp=sharing) contains the most up to date agenda for our in-person Sales Quick Start Workshop. Please check the tabs at the bottom of this document to see each day's agenda. This agenda is subject to change based on the individual needs of the class and the availability of SMEs, but we will make every effort to surface those changes in this document.


## SQS Remote Agenda:

### SQS 20 - January 2022

| DATE | START TIME | END TIME | ACTIVITY | SME ASSIGNED |
| ------ | ------ | ------ | ------ | ------ |
|January 12, 2022 | 10:00a ET | 11:20a ET | Essential Questions Exercise | Field Enablement  |
|January 12, 2022 | 11:30a ET | 12:50p ET | Value Card Exercise | Field Enablement |
|January 13, 2022 | 10:00a ET | 11:20a ET | Discovery Question Exercise | Field Enablement  |
|January 13, 2022 | 11:30a ET | 12:50p ET | Differentiator Exercise | Field Enablement  |
|January 14, 2022 | 11:00a ET | 12:20p ET | MEDDPPICC & Breakout Call Prep | Field Enablement  |
|January 14, 2022  | 12:30p ET | 1:20p ET | No Tissues for Issues + Product Tiering | Product Marketing  |
|January 18, 2022 | 11:00a ET | 11:50a ET | Discovery Call 1 | Mock Customers  |
|January 19, 2022 | 11:00a ET | 11:50a ET | Discovery Call 2 | Mock Customers |
|January 20, 2022 | 12:00p ET | 12:50p ET | Discovery Call 3 | Mock Customers |
|January 24, 2022 | 11:00a ET | 11:50a ET | Discovery Call 4 | Mock Customers |
|January 18, 2022 | 12:00p ET | 12:30p ET | Field Security |  Field Security Team   |
|January 19, 2022 | 11:00a ET | 11:50a ET | Intro to Competition | Competitive Intelligence  |
|January 20, 2022 | 10:00a ET | 10:40a ET | Legal / Deal Desk | Legal & Deal Desk Team |
|January 21, 2022 | 11:00a ET | 11:50a ET | Discussion: Professional Services | Professional Services Team    |
|January 21, 2022 | 12:00p ET | 12:50p ET | Channels |  Channel Team   |
|January 24, 2022 | 12:00p ET | 12:50p ET | Alliances | Alliances Team  |

### SQS 21 - February 2022

| DATE | START TIME | END TIME | ACTIVITY | SME ASSIGNED |
| ------ | ------ | ------ | ------ | ------ |
|February 16, 2022 | 10:00a ET | 11:20a ET | Essential Questions Exercise | Field Enablement  |
|February 16, 2022 | 11:30a ET | 12:50p ET | Value Card Exercise | Field Enablement  |
|February 17, 2022 | 10:00a ET | 11:20a ET | Discovery Question Exercise | Field Enablement |
|February 17, 2022 | 11:30a ET | 12:50p ET | Differentiator Exercise | Field Enablement  |
|February 18, 2022 | 11:00a ET | 12:20p ET | MEDDPPICC & Breakout Call Prep |  Field Enablement |
|February 18, 2022  | 12:30p ET | 1:20p ET | No Tissues for Issues + Product Tiering | Product Marketing  |
|February 21, 2022 | 11:00a ET | 11:50a ET | Discovery Call 1 |  Mock Customers  |
|February 22, 2022 | 11:00a ET | 11:50a ET | Discovery Call 2 |  Mock Customers |
|February 21, 2022  | 12:00p ET | 12:50p ET | Alliances | Alliances Team  |
|February 23, 2022 | 12:00p ET | 12:50p ET | Discovery Call 3 |  Mock Customers |
|February 24, 2022 | 11:00a ET | 11:50a ET | Discovery Call 4 |  Mock Customers |
|February 22, 2022 | 12:00p ET | 12:30p ET | Field Security | Field Security Team    |
|February 23, 2022 | 11:00a ET | 11:50a ET | Intro to Competition | Competitive Intelligence  |
|February 24, 2022 | 10:00a ET | 10:40a ET | Legal / Deal Desk | Legal & Deal Desk Team |
|February 28, 2022 | 11:00a ET | 11:50a ET | Discussion: Professional Services |  Professional Services Team    |
|February 28, 2022 | 12:00p ET | 12:50p ET | Channels | Channel Team  |

