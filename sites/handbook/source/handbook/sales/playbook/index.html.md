---
layout: handbook-page-toc
title: "Enterprise Sales"
description: "The Enterprise Sales department at GitLab focuses on delivering maximum value to strategic and large prospects and customers throughout their entire journey with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
Welcome to the Enterprise Sales handbook page!

The Enterprise Sales department is part of [GitLab Sales](https://about.gitlab.com/handbook/sales/#welcome-to-the-sales-team-homepage). The sales field in Enterprise is made up of [Strategic Account Leaders (SALs)](https://about.gitlab.com/job-families/sales/strategic-account-leader/) who work across functions deliver maximum value to strategic and large prospects and customers throughout their entire journey with GitLab.

Besides this page, there are a few bookmarks you’ll want to set that will be your main sources of truth during your everyday work. These are:

- [The GitLab Sales handbook page](https://about.gitlab.com/handbook/sales/#welcome-to-the-sales-team-homepage): This page serves as our home base. From here, you can find a wealth of resources that are relevant to the entire sales field. Whether you’re looking for sales order processing info, or wondering how to get executive support, this is where you’ll find what you need.
- [Field Slack Channels](https://about.gitlab.com/handbook/sales/sales-google-groups/#field-slack-channels): View and join these Slack channels based on your role and team to stay connected with your peers.
- [The SAL learning hub](https://gitlab.edcast.com/channel/strategic-account-leaders): You’ll receive access to EdCast during your first week. Follow the Enterprise SAL learning channel in EdCast to see all training relevant to your role in one place. This includes training and refresher courses on our sales methodology, tools, and sales skills. 

## The SAL playbook
The below table shows major strategic resources that can help you be successful during each milestone of a deal - from prospecting to transitioning to a the post-sales team. For operational resources, head to the general sales page up top. For commonly used sales assets like marketing plays and pitch decks, head to the [marketing resources handbook page](https://about.gitlab.com/handbook/marketing/strategic-marketing/sales-resources/).


| Educate & Engage<br>(Sales Stages 0-3) | Facilitate the Opportunity<br>(Sales Stages 3-4) | Deal Closure<br>(Sales Stages 5-6) | Retain and Expand<br>(Growth) |
| ------ | ------ | ------ | ------ |
| * [Prospecting](/handbook/sales/prospecting/)<br> * [Discovery](/handbook/sales/playbook/discovery/)<br> * [Articulate Value](https://about.gitlab.com/handbook/sales/command-of-the-message/) | * [Position to Win](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#step-2-scoping)<br> * [Technical Evaluation](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#step-3-technical-evaluation)<br> * [Customer-Centric Proposal](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#12-creating-the-customer-deck-phase-1) | * [Mutual Close Plan](/handbook/sales/mutual-close-plan)<br> * [Negotiate to Close](/handbook/sales/negotiate-to-close/)<br> * [Order Processing](/handbook/sales/field-operations/order-processing/) | * [Account Transition](/handbook/customer-success/pre-sales-post-sales-transition/)<br> * [Customer Onboarding](/handbook/customer-success/tam/onboarding/) |

View the [Commercial Sales Playbook](https://about.gitlab.com/handbook/sales/commercial/) here.
